# MADS - BookBandy
 ### Theme: 
 
 ### Repository organization

 
 ### Gallery
 | | |
 |:-:|:-:|
 | | |

 
 ### Frameworks and Libraries
 * [Laravel](https://github.com/laravel/laravel)
 * [Vue](https://vuejs.org/)

### Technologies
 * PHP
 * mySQL
 * VueJs
 * Javascript
 * HTML + CSS
 
### Artifacts
#### Requirements Specification and User Stories
* 1.1: [Requirements Specification](https://gitlab.com/jflcarvalho/mdas-feup/wikis/Requirements-Specification)
* 1.2: [User Stories](https://gitlab.com/jflcarvalho/mdas-feup/wikis/User-Stories)

 ### Developers:
  * João Carvalho [@jflcarvalho](https://gitlab.com/jflcarvalho) - up201504875@fe.up.pt
  * Igor Silveira [@igorsilveira](https://gitlab.com/igorsilveira) - up201504875@fe.up.pt
