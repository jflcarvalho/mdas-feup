FROM php:7.2-fpm-alpine

# Install dependencies
RUN apt-get update
RUN docker-php-ext-install pdo pdo_pgsql pgsql

# Install Composer
# RUN curl --silent --show-error https://getcomposer.org/installer | php
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer config --global process-timeout 2000 # Prevents timeout issues

RUN php artisan serve --host=0.0.0.0