@extends('common.base')

@section('content')
    <div class="position-relative">
        <!-- shape Hero -->
        <section class="section-shaped my-0">
            <div class="shape shape-style-1 shape-default shape-skew">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="container shape-container d-flex">
                <div class="col px-0">
                    <div class="row">
                        <div class="col-lg-6">
                            <h1 class="display-3  text-white">Exchange Your Old Books
                                <span>don't make trash, share</span>
                            </h1>
                            <p class="lead  text-white">The main goal of BookBandy is to establish
                                a way of user trade their own books with other users. </p>
                            <div class="btn-wrapper">
                                <base-button tag="a"
                                                href="https://demos.creative-tim.com/argon-design-system/docs/components/alerts.html"
                                                class="mb-3 mb-sm-0"
                                                type="info"
                                                icon="ni ni-books">
                                    Join Us
                                </base-button>
                                <base-button tag="a"
                                                href="#community"
                                                class="mb-3 mb-sm-0"
                                                type="white"
                                                icon="ni ni-single-02">
                                    About Community
                                </base-button>
                            </div>
                        </div>
                        <div class="col-lg-6 pt-5" style="font-size:4.5em">
                            <div class="d-flex flex-row align-items-center text-white">
                                <i class="flex-fill text-right fas fa-book"></i>
                                <i class="flex-fill text-center fas fa-exchange-alt"></i>
                                <i class="flex-fill fas fa-book"></i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- 1st Hero Variation -->
    </div>
    <section class="section section-lg pt-lg-0 mt--200">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="row row-grid">
                        <div class="col-lg-4">
                            <card class="border-0" hover shadow body-classes="py-5">
                                <icon name="fas fa-book-medical" type="primary" rounded class="mb-4">
                                </icon>
                                <h6 class="text-primary text-uppercase">Add Your Books</h6>
                                <p class="description mt-3">Are you seeing dust in the bookshelf?
                                    <strong>Bring a new life to your books</strong> and help the others sharing the written knowledge.
                                </p>
                                <div>
                                    <badge type="primary" rounded>old</badge>
                                    <badge type="primary" rounded>newlife</badge>
                                    <badge type="primary" rounded>read</badge>
                                </div>
                                <base-button tag="a" href="/signup" type="primary" class="mt-4">
                                    Add Books
                                </base-button>
                            </card>
                        </div>
                        <div class="col-lg-4">
                            <card class="border-0" hover shadow body-classes="py-5">
                                <icon name="fa fa-search" type="success" rounded class="mb-4">
                                </icon>
                                <h6 class="text-success text-uppercase">Search Other Books</h6>
                                <p class="description mt-3">You don't have any more books to read in home?
                                    Use BookBandy and start searching the next one in our database.</p>
                                <div>
                                    <badge type="success" rounded>search</badge>
                                    <badge type="success" rounded>books</badge>
                                    <badge type="success" rounded>database</badge>
                                </div>
                                <base-button tag="a" href="#" type="success" class="mt-4">
                                    Search Now
                                </base-button>
                            </card>
                        </div>
                        <div class="col-lg-4">
                            <card class="border-0" hover shadow body-classes="py-5">
                                <icon name="fas fa-exchange-alt" type="warning" rounded class="mb-4">
                                </icon>
                                <h6 class="text-warning text-uppercase">Swap With Ohters</h6>
                                <p class="description mt-3">After you now what book you want next to read,
                                    start <strong>trading your own books</strong> with other people by making a offer to then.
                                </p>
                                <div>
                                    <badge type="warning" rounded>exchange</badge>
                                    <badge type="warning" rounded>users</badge>
                                    <badge type="warning" rounded>negotiate</badge>
                                </div>
                                <base-button tag="a" href="#" type="warning" class="mt-4">
                                    Start Trade
                                </base-button>
                            </card>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- TODO: Change Image -->
    <section class="section section-lg">
        <div class="container">
            <div class="row row-grid align-items-center">
                <div class="col-md-6 order-md-2">
                    <img src="img/theme/promo-1.png" class="img-fluid floating">
                </div>
                <div class="col-md-6 order-md-1">
                    <div class="pr-md-5">
                        <icon name="fa fa-search" class="mb-5" size="lg" type="success" shadow
                                rounded></icon>
                        <h3>Searching features</h3>
                        <p>The BookBandy comes with a variety of <strong>searching filters</strong> to help you find the right book. You can change
                            the filter very easily by:</p>
                        <ul class="list-unstyled ml-3">
                            <li class="py-2">
                                <div class="d-flex align-items-center">
                                    <badge type="success" circle class="mr-3" icon="fas fa-heading"></badge>
                                    <h6 class="mb-0">Titles</h6>
                                </div>
                            </li>
                            <li class="py-2">
                                <div class="d-flex align-items-center">
                                    <badge type="success" circle class="mr-3" icon="fas fa-pen-fancy"></badge>
                                    <h6 class="mb-0">Authors</h6>
                                </div>
                            </li>
                            <li class="py-2">
                                <div class="d-flex align-items-center">
                                    <badge type="success" circle class="mr-3" icon="fas fa-archive"></badge>
                                    <h6 class="mb-0">Categories</h6>
                                </div>
                            </li>
                            <li class="py-2">
                                <div class="d-flex align-items-center">
                                    <badge type="success" circle class="mr-3" icon="fas fa-print"></badge>
                                    <h6 class="mb-0">Publisers</h6>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- TODO: remove lorem ipsum -->
    <section id="community" class="section bg-secondary">
        <div class="container">
            <div class="row row-grid align-items-center">
                <div class="col-md-6">
                    <div class="card bg-default shadow border-0">
                        <img v-lazy="'img/theme/img-1-1200x1000.jpg'" class="card-img-top">
                        <blockquote class="card-blockquote">
                            <svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 583 95"
                                    class="svg-bg">
                                <polygon points="0,52 583,95 0,95" class="fill-default"></polygon>
                                <polygon points="0,42 583,95 683,0 0,95" opacity=".2" class="fill-default"></polygon>
                            </svg>
                            <h4 class="display-3 font-weight-bold text-white">Design System</h4>
                            <p class="lead text-italic text-white">Proin quis est a tellus pretium faucibus eu quis ex.
                                Nulla velit erat, pellentesque quis mollis id, congue non libero.
                                Fusce tristique dignissim.</p>
                        </blockquote>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pl-md-5">
                        <icon name="fas fa-user-friends" class="mb-5" size="lg" type="warning" shadow rounded></icon>

                        <h3>Our Community</h3>
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dui neque,
                            dictum id sollicitudin et, mollis maximus risus.</p>
                        <p>Nullam suscipit aliquam vehicula. Duis id ligula porta, tincidunt diam ut, venenatis lacus.
                            Vivamus nibh elit, bibendum ac mauris placerat, convallis aliquam sem.</p>
                        <p>Praesent ultricies sed magna quis imperdiet. Donec euismod dignissim elit, nec pellentesque metus aliquam non.
                            Donec dictum ornare nibh, eu maximus ligula semper et. </p>
                        <a href="#" class="font-weight-bold text-warning mt-5">
                                Sed vestibulum, mauris sit amet pulvinar</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- TODO: Change Lorem Ipsum-->
    <section class="section section section-shaped my-0 overflow-hidden">
        <div class="shape shape-style-1 bg-gradient-warning shape-skew">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="container py-0">
            <div class="row row-grid align-items-center">
                <div class="col-md-5 order-lg-2 ml-lg-auto">
                    <div class="position-relative pl-md-4">
                        <img src="img/ill/ill-2.svg" class="img-center img-fluid">
                    </div>
                </div>
                <div class="col-lg-7 order-lg-1">
                    <div class="d-flex px-3">
                        <div>
                            <icon name="ni ni-building" size="lg" class="bg-gradient-white" color="primary" shadow
                                    rounded></icon>
                        </div>
                        <div class="pl-4">
                            <h4 class="display-3 text-white">Modern Interface</h4>
                            <p class="text-white">Nullam suscipit aliquam vehicula. Duis id ligula porta,
                                tincidunt diam ut, venenatis lacus. Vivamus nibh elit, bibendum ac mauris
                                placerat, convallis aliquam sem.</p>
                        </div>
                    </div>
                    <card shadow class="shadow-lg--hover mt-5">
                        <div class="d-flex px-3">
                            <div>
                                <icon name="ni ni-satisfied" gradient="success" color="white" shadow
                                        rounded></icon>
                            </div>
                            <div class="pl-4">
                                <h5 class="title text-success">Easiest Way to Add Books</h5>
                                <p>To add your books to our platform you just need to scan your books ISBN with the
                                    camera of your smartphone or upload a picture from your PC, and you can leave
                                    the rest with us.</p>
                                <i style="font-size: 4.5em;
                                position: absolute;
                                bottom: 1.5rem;
                                left: 1.5rem;" class="fas fa-barcode"></i>
                            </div>
                        </div>
                    </card>
                    <card shadow class="shadow-lg--hover mt-5">
                        <div class="d-flex px-3">
                            <div>
                                <icon name="ni ni-active-40" gradient="warning" color="white" shadow
                                        rounded></icon>
                            </div>
                            <div class="pl-4">
                                <h5 class="title text-warning">Lorem Ipsum</h5>
                                <p>Nullam suscipit aliquam vehicula. Duis id ligula porta, tincidunt diam ut,
                                    venenatis lacus. Vivamus nibh elit, bibendum ac mauris placerat, convallis
                                    aliquam sem.</p>
                                <a href="#" class="text-warning">Learn more</a>
                            </div>
                        </div>
                    </card>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-lg">
        <div class="container">
            <div class="row justify-content-center text-center mb-lg">
                <div class="col-lg-8">
                    <h2 class="display-3">The amazing Team</h2>
                    <p class="lead text-muted">Two brilliant students on their final year of Master Degree,
                        that accept the challenge to create something new, something helpful to everyone.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-5 mb-lg-0">
                    <div class="px-4">
                        <img v-lazy="'img/theme/igor-team.jpg'"
                                class="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                style="width: 200px;">
                        <div class="pt-4 text-center">
                            <h5 class="title">
                                <span class="d-block mb-1">Igor Silveira</span>
                                <small class="h6 text-muted">Full-Stack Developer</small>
                            </h5>
                            <div class="mt-3">
                                <base-button tag="a" href="https://www.instagram.com/igorasilveira/" type="info" icon="fab fa-instagram" rounded
                                                icon-only></base-button>
                                <base-button tag="a" href="https://www.facebook.com/IgorSilveira00" type="info" icon="fab fa-facebook" rounded
                                                icon-only></base-button>
                                <base-button tag="a" href="https://gitlab.com/igorsilveira" type="info" icon="fab fa-gitlab" rounded
                                                icon-only></base-button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-5 mb-lg-0">
                    <div class="px-4">
                        <img v-lazy="'img/theme/joao-team.jpg'"
                                class="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                style="width: 200px;">
                        <div class="pt-4 text-center">
                            <h5 class="title">
                                <span class="d-block mb-1">João Carvalho</span>
                                <small class="h6 text-muted">Full-Stack Developer</small>
                            </h5>
                            <div class="mt-3">
                                <base-button tag="a" href="https://www.instagram.com/jflcarvalho/" type="success" icon="fab fa-instagram" rounded
                                                icon-only></base-button>
                                <base-button tag="a" href="https://www.facebook.com/profile.php?id=100000291598434" type="success" icon="fab fa-facebook" rounded
                                                icon-only></base-button>
                                <base-button tag="a" href="https://gitlab.com/jflcarvalho" type="success" icon="fab fa-gitlab" rounded
                                                icon-only></base-button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- TODO: Fix all Content -->
    <section class="section section-shaped my-0 overflow-hidden">
        <div class="shape shape-style-3 bg-gradient-default shape-skew">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="container pt-lg pb-300">
            <div class="row text-center justify-content-center">
                <div class="col-lg-10">
                    <h2 class="display-3 text-white">Build something</h2>
                    <p class="lead text-white">According to the National Oceanic and Atmospheric Administration,
                        Ted, Scambos, NSIDClead scentist, puts the potentially record low maximum sea ice extent
                        tihs year down to low ice.</p>
                </div>
            </div>
            <div class="row row-grid mt-5">
                <div class="col-lg-4">
                    <icon name="ni ni-settings" size="lg" gradient="white" shadow round color="primary"></icon>
                    <h5 class="text-white mt-3">Building tools</h5>
                    <p class="text-white mt-3">Some quick example text to build on the card title and make up the
                        bulk of the card's content.</p>
                </div>
                <div class="col-lg-4">
                    <icon name="ni ni-ruler-pencil" size="lg" gradient="white" shadow round color="primary"></icon>
                    <h5 class="text-white mt-3">Grow your market</h5>
                    <p class="text-white mt-3">Some quick example text to build on the card title and make up the
                        bulk of the card's content.</p>
                </div>
                <div class="col-lg-4">
                    <icon name="ni ni-atom" size="lg" gradient="white" shadow round color="primary"></icon>
                    <h5 class="text-white mt-3">Launch time</h5>
                    <p class="text-white mt-3">Some quick example text to build on the card title and make up the
                        bulk of the card's content.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-md pt-lg-0 section-contact-us">
        <div class="container">
            <div class="row justify-content-center mt--300">
                <div class="col-lg-8">
                    <card gradient="secondary" shadow body-classes="p-lg-5">
                        <h4 class="mb-1">Contact us?</h4>
                        <p class="mt-0">Your ideias are very important to us.</p>
                        <base-input class="mt-5"
                                    alternative
                                    placeholder="Your name"
                                    addon-left-icon="ni ni-user-run">
                        </base-input>
                        <base-input alternative
                                    placeholder="Email address"
                                    addon-left-icon="ni ni-email-83">
                        </base-input>
                        <base-input class="mb-4">
                                <textarea class="form-control form-control-alternative" name="name" rows="4"
                                            cols="80" placeholder="Type a message..."></textarea>
                        </base-input>
                        <base-button type="default" round block size="lg">
                            Send Message
                        </base-button>
                    </card>
                </div>
            </div>
        </div>
    </section>
@endsection
