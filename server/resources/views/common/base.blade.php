<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'BookBandy') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}"/>


        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
        ]) !!};
        </script>
    </head>
    <body>

        <div id="app">
            @include('common.header')

            <main>
                @yield('content')
            </main>

            @include('common.footer')
        </div>

        <script src={{ asset('js/app.js') }}></script>
    </body>
</html>
