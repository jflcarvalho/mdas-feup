import Vue from 'vue'

import AppHeader from "./layout/AppHeader"
import AppFooter from "./layout/AppFooter"
import { FadeTransition } from "vue2-transitions";
import Argon from './plugins/argon-kit'
import "./registerServiceWorker"

Vue.use(Argon);

Vue.component('app-header', AppHeader);
Vue.component('app-footer', AppFooter);

new Vue({
    el: "#app",
    delimiters: ['${', '}'],
    components: {
        FadeTransition
    }
});